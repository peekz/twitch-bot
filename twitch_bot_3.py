#!/usr/bin/python3
# bot ver. 1.1
import socket
import sys
import time

# https://www.twitchapps.com/tmi/ => twitch auth token

CGREEN = '\033[32m'
CEND = '\033[0m'

if len(sys.argv) < 4:
	print(CGREEN + "\n[!] Usage: python3 " + sys.argv[0] + " <nick> <channel> <oauth:token>\n" + CEND)
	sys.exit()
HOST = "irc.chat.twitch.tv"
PORT = 6667
BOTNAME =  sys.argv[1]
CHAN = sys.argv[2]
PASS = sys.argv[3]

def chat(sock, msg):
	sock.send("PRIVMSG #{} :{}\r\n".format(CHAN,msg).encode("utf-8"))

def main():
	print(CHAN)
	s = socket.socket()
	s.connect((HOST,PORT))
	s.send("PASS {}\r\n".format(PASS).encode("utf-8"))
	s.send("NICK {}\r\n".format(BOTNAME).encode("utf-8"))
	s.send("JOIN #{}\r\n".format(CHAN).encode("utf-8"))

	chat(s,"Bonj0ur ! je suis un gentil b0t")
	while True:
		response = s.recv(1024).decode("utf-8")
		#print(response)
		if response == "PING :tmi.twitch.t\r\n":
			s.send("PONG :tmi.twitch.t\r\n".encode("utf-8"))
		else:
			username = response[1:].split("!")[0]
			message = response.split("#{} :".format(CHAN))[-1]
			print("{}>{}".format(username,message))
			if message == "café\r\n":
		    	    chat(s,"Je bois un grand café au lait")


main()
